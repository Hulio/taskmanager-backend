import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import { AuthDto } from './dto/auth.dto';
import { verify } from 'argon2';
import { omit } from 'lodash';
import { CookieOptions, Response } from 'express';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  COOKIE_REFRESH_TOKEN_NAME = 'refreshToken';
  COOKIE_OPTIONS: Partial<CookieOptions> = {
    httpOnly: true,
    sameSite: 'none',
  };
  constructor(
    private jwt: JwtService,
    private userService: UserService,
    private configService: ConfigService,
  ) {}

  async login(dto: AuthDto) {
    const user = omit(await this.validateUser(dto), ['password']);

    const tokens = this.issueTokens(user.id);
    return {
      user,
      ...tokens,
    };
  }

  async register(dto: AuthDto) {
    const isExistsUser = await this.userService.getByEmail(dto.email);
    if (isExistsUser) throw new BadRequestException('User already exists');

    const user = await this.userService.create(dto);

    const tokens = this.issueTokens(user.id);

    return {
      user,
      ...tokens,
    };
  }

  private issueTokens(userId: string) {
    const data = { id: userId };
    const accessToken = this.jwt.sign(data, {
      expiresIn: '15m',
    });

    const refreshToken = this.jwt.sign(data, {
      expiresIn: '7d',
    });

    return {
      accessToken,
      refreshToken,
    };
  }
  private async validateUser(dto: AuthDto) {
    const user = await this.userService.getByEmail(dto.email);

    if (!user) throw new NotFoundException('User not found');

    const isValid = await verify(user.password, dto.password);

    if (!isValid) throw new NotFoundException('Invalid password');

    return user;
  }

  addRefreshTokenToResponse(res: Response, refreshToken: string) {
    const expiresIn = new Date();
    expiresIn.setDate(expiresIn.getDate() + 1);

    res.cookie(this.COOKIE_REFRESH_TOKEN_NAME, refreshToken, {
      ...this.COOKIE_OPTIONS,
      expires: expiresIn,
    });
  }

  removeRefreshToken(res: Response) {
    res.cookie(this.COOKIE_REFRESH_TOKEN_NAME, 'val', {
      ...this.COOKIE_OPTIONS,
      expires: new Date(0),
    });
  }
  async getNewTokens(token) {
    const data = this.jwt.verify(token);
    if (!data) throw new UnauthorizedException('Invalid Token');

    const user = omit(await this.userService.getById(data.id), ['password']);
    const tokens = this.issueTokens(user.id);

    return {
      user,
      ...tokens,
    };
  }
}
